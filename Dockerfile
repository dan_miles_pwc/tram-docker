FROM python:3.8-slim

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

WORKDIR /tram

RUN git clone https://github.com/mitre-attack/tram.git && \
    cd tram && \
    pip install -r requirements.txt

WORKDIR /tram/tram

COPY load_model.py .
RUN python load_model.py

COPY config.yml /tram/tram/conf/
COPY enterprise-attack.json /tram/tram/models/

EXPOSE 9999

CMD ["python", "tram.py"]